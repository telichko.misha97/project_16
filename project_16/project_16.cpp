﻿#include <iostream>
#include <ctime>

using namespace std;


int main()
{
	const int N = 3;

	time_t now = time(0);
	struct tm timeinfo;
	localtime_s(&timeinfo, &now);
	int iday = timeinfo.tm_mday;

	int array[N][N];
	// переменная x отвечает за индекс строки массива.
	int x = iday % N;
	int sum = 0;

	for (int i = 0; i < N; i++) 
	{
		//sum = sum + i;

		for (int j = 0; j < N; j++)
		{

			array[i][j] = i + j;

			if (i == x)
			{
				sum = sum + array[i][j];
			}
			cout << array[i][j] << "\t";
		}
		cout << endl;
	}
	cout << sum << "\n"; // вывод суммы элементов строки 
	cout << "x = " << x; // проверка индекса 
}
